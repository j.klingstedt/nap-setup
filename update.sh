#!/bin/bash

rm app-protect-security-updates.key

printf "deb https://pkgs.nginx.com/app-protect-security-updates/ubuntu/ $(lsb_release -cs) nginx-plus\n" | sudo tee /etc/apt/sources.list.d/app-protect-security-updates.list

sudo wget https://cs.nginx.com/static/keys/app-protect-security-updates.key && sudo apt-key add app-protect-security-updates.key

sudo apt-get update

# Install the latest attack signatures and threat campaigns
sudo apt-get -y install app-protect-attack-signatures
sudo apt-get -y install app-protect-threat-campaigns

# restart nginx
sudo systemctl restart nginx
