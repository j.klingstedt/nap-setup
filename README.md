# Getting Started with NAP Setup

1. Checkout this repo somewhere within your home directory
2. From your `devenv-agent` VM, navigate to this directory: `cd ~/host_user_homedir/<wherever-this-resides>`
3. Run `sh setup.sh`

This should run all of the commands needed to get NAP running on your VM.

**Note: `nginx.conf` and `nginx-agent.conf` will overwrite whatever is currently in place. This is intended for new VMs that have not been altered.**

## Extras

1. `sh remove.sh` will remove `app-protect` and restart nginx.
2. `sh re-install.sh` will re-install `app-protect` and restart nginx.
3. `sh update` will install `app-protect-attack-signatures` and `app-protect-threat-campaigns` and restart nginx.
