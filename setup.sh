#!/bin/bash

# cleanup
sudo rm nginx_signing.key

# copy certs
sudo mkdir -p /etc/ssl/nginx
sudo cp certs/nginx-repo.crt /etc/ssl/nginx
sudo cp certs/nginx-repo.key /etc/ssl/nginx

# install apt utils
sudo apt-get update && sudo apt-get install apt-transport-https lsb-release ca-certificates wget gnupg2

# download and add signing key
sudo wget https://cs.nginx.com/static/keys/nginx_signing.key && sudo apt-key add nginx_signing.key

# Remove any previous NGINX Plus repository and apt configuration files:
sudo rm /etc/apt/sources.list.d/nginx-plus.list
sudo rm /etc/apt/apt.conf.d/90nginx

#Add NGINX Plus repository
printf "deb https://pkgs.nginx.com/plus/ubuntu $(lsb_release -cs) nginx-plus\n" | sudo tee /etc/apt/sources.list.d/nginx-plus.list

# Add NGINX App Protect WAF repository
printf "deb https://pkgs.nginx.com/app-protect/ubuntu $(lsb_release -cs) nginx-plus\n" | sudo tee /etc/apt/sources.list.d/nginx-app-protect.list

# Download the apt configuration to /etc/apt/apt.conf.d:
sudo wget -P /etc/apt/apt.conf.d https://cs.nginx.com/static/files/90pkgs-nginx

#Update the repository and install the most recent version of the NGINX App Protect WAF package (which includes NGINX Plus):
sudo apt-get update
sudo apt-get -y install nginx-plus-r26
sudo systemctl restart nginx

# install app-protect
sudo apt-get -y install app-protect
sudo systemctl restart nginx

# update nginx config
sudo cp nginx.conf /etc/nginx

# add app protect policy file
sudo cp my-nap-policy.json /etc/app_protect/
sudo systemctl restart nginx

# install agent and update config
curl -k https://192.168.56.3/install/nginx-agent | sudo sh
# copy conf file with load module and other stuff needed for app protect
sudo cp nginx-agent.conf /etc/nginx-agent
sudo systemctl start nginx-agent
