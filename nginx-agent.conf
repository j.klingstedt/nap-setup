#
# /etc/nginx-agent/nginx-agent.conf
#
# Configuration file for NGINX Agent.
#
# This file is to track agent configuration values that are meant to be statically set. There  
# are additional agent configuration values that are set via the API and agent install script
# which can be found in /etc/nginx-agent/agent-dynamic.conf. 

# specify the server grpc port to connect to
server:
  # host of the control plane
  host: 192.168.56.3
  grpcPort: 443
  # provide servername overrides if using SNI
  # metrics: ""
  # command: ""
# tls options
tls:
  # enable tls in the nginx-agent setup for grpcs
  # default to enable to connect with secure connection but without client cert for mtls
  enable: true
  # specify the absolute path to the CA file, when mtls is enabled
  # ca: /etc/nginx-agent/ca.pem
  # specify the absolute path to the client cert, when mtls is enabled
  # cert: /etc/nginx-agent/manager-client.crt
  # specify the absolute path to the client cert key, when mtls is enabled
  # key: /etc/nginx-agent/manager-client.crt
  # controls whether the server certificate chain and host name are verified.
  # for production use, see instructions for configuring TLS
  skip_verify: true
log:
  # set log level (panic, fatal, error, info, debug, trace; default "info")
  level: info
  # set log path. if empty, don't log to file.
  path: /var/log/nginx-agent/
# data plane status message / 'heartbeat'
nginx:
  # path of NGINX logs to exclude
  exclude_logs: ""

dataplane:
  sync: 
    enable: true
  status:
    # poll interval for data plane status - the frequency the agent will query the dataplane for changes
    poll_interval: 30s
    # report interval for data plane status - the maximum duration to wait before syncing dataplane information if no updates have being observed
    report_interval: 24h
  events:
    # report data plane events back to the control plane
    enable: true
metrics:
  # specify the size of a buffer to build before sending metrics
  bulk_size: 20
  # specify metrics poll interval
  report_interval: 1m
  collection_interval: 15s
  mode: aggregated

# OSS NGINX default config path
# path to aux file dirs can also be added
config_dirs: "/etc/nginx:/usr/local/etc/nginx:/usr/share/nginx/modules:/etc/nms/:/etc/app_protect"

nginx_app_protect:
  report_interval: 15s
